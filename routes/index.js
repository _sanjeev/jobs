const express = require("express");
const { auth } = require("../middlewares/auth");
const router = express.Router();

router.use("/api/v1", require("./auth"));
router.use("/api/v1", auth, require("./jobs"));

module.exports = router;
