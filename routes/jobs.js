const express = require("express");
const { createJobs, getAllJobs, getJob, updateJob, deleteJob } = require("../controllers/jobs");
const router = express.Router();

router.post("/job", createJobs);
router.get("/jobs", getAllJobs);
router.get("/job/:id", getJob);
router.patch("/job/:jobId", updateJob);
router.delete("/job/:jobId", deleteJob);

module.exports = router;
