const express = require("express");
const dotenv = require("dotenv");
const { notFound } = require("./middlewares/notFound");
const { errorHandler } = require("./middlewares/errorHandler");
const connectDB = require("./db/connect");

const helmet = require("helmet");
const cors = require("cors");
const xss = require("xss-clean");
const rateLimiter = require("express-rate-limit");

const swaggerUI = require("swagger-ui-express");
const YAML = require("yamljs");
const swaggerDocument = YAML.load("./swagger.yaml");

dotenv.config();
require("express-async-errors");

const app = express();

app.set("trust proxy", 1);
app.use(
    rateLimiter({
        windowMs: 60 * 1000,
        max: 60,
    })
);
app.use(express.json());
app.use(helmet());
app.use(cors());
app.use(xss());
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocument));
app.use("/", require("./routes"));
app.use(notFound);
app.use(errorHandler);

const port = process.env.PORT || 5000;

const start = async () => {
    try {
        await connectDB(process.env.MONGO_URI);
        app.listen(port, (err) => {
            if (err) {
                console.error(err);
                return;
            }
            console.log(`Server is listening on port ${port}`);
        });
    } catch (error) {
        console.error(error);
    }
};

start();
