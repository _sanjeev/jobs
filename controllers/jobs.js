const Job = require("../models/Job");
const { StatusCodes } = require("http-status-codes");
const { BadRequestError, NotFoundError } = require("../errors");

module.exports.getAllJobs = async (req, res, next) => {
    const allJobs = await Job.find({
        createdBy: req.user.id,
    }).sort("createdAt");

    return res.status(StatusCodes.OK).json({
        status: true,
        message: "Successfully get all the jobs!",
        entity: allJobs,
        count: allJobs.length,
    });
};

module.exports.createJobs = async (req, res, next) => {
    req.body.createdBy = req.user.id;
    const job = await Job.create(req.body);

    if (!job) throw new NotFoundError("Job not found!");

    return res.status(StatusCodes.CREATED).json({
        status: true,
        message: "Successfully created jobs!",
        entity: job,
    });
};

module.exports.getJob = async (req, res, next) => {
    const id = req.params.id;
    const job = await Job.findOne({
        _id: id,
        createdBy: req.user.id,
    });

    if (!job) throw new NotFoundError(`No job with id ${id}`);

    return res.status(StatusCodes.OK).json({
        status: true,
        message: "Successfully get job!",
        entity: job,
    });
};

module.exports.updateJob = async (req, res, next) => {
    const {
        user: { id },
        params: { jobId },
        body: { company, position, status },
    } = req;

    if (!company || !position || !status) throw new BadRequestError("Company, Position or Status fields cannot be empty!");

    const job = await Job.findOneAndUpdate(
        {
            _id: jobId,
            createdBy: id,
        },
        req.body,
        { new: true, runValidators: true }
    );

    if (!job) throw new NotFoundError("Job not found!");

    return res.status(StatusCodes.OK).json({
        status: true,
        message: "Successfully updated the job!",
        entity: job,
    });
};

module.exports.deleteJob = async (req, res, next) => {
    const {
        params: { jobId },
        user: { id },
    } = req;

    const job = await Job.findByIdAndRemove({
        _id: jobId,
        createdBy: id,
    });

    if (!job) throw new NotFoundError("Job not found");

    return res.status(StatusCodes.OK).json({
        status: false,
        message: "Successfully delete the job!",
        entity: job,
    });
};
