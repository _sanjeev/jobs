const User = require("../models/User");
const { StatusCodes } = require("http-status-codes");
const { BadRequestError, UnAuthenticatedError } = require("../errors");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

module.exports.register = async (req, res, next) => {
    const { name, email, password } = req.body;

    // if (!name || !email || !password) throw new BadRequestError("Please provide name email and password");

    // const salt = await bcrypt.genSalt(10);
    // const hashedPassword = await bcrypt.hash(password, salt);

    // const tempUser = {
    //     name,
    //     email,
    //     password: hashedPassword,
    // };

    const user = await User.create({ name, email, password });
    // const token = jwt.sign(
    //     {
    //         id: user._id,
    //         name: user.name,
    //         email: user.email,
    //     },
    //     process.env.JWT_SECRET,
    //     {
    //         expiresIn: "30d",
    //     }
    // );

    const token = user.createJWTToken();
    return res.status(StatusCodes.CREATED).json({
        status: true,
        message: "Successfully register the user!",
        entity: {
            user,
            token,
        },
    });
};

module.exports.login = async (req, res, next) => {
    const { email, password } = req.body;

    if (!email || !password) throw new BadRequestError("Please provide email and password");

    const user = await User.findOne({
        email: email,
    });

    if (!user) throw new UnAuthenticatedError("Invalid Credentials");

    const isPasswordCorrect = await user.comparePassword(password);

    if (!isPasswordCorrect) throw new UnAuthenticatedError("Invalid Credentials");

    const token = user.createJWTToken();

    return res.status(StatusCodes.OK).json({
        status: false,
        message: "Successfully login the user",
        entity: {
            user,
            token,
        },
    });
};
