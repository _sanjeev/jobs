const { UnAuthenticatedError } = require("../errors");
const jwt = require("jsonwebtoken");
const User = require("../models/User");

module.exports.auth = async (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (!authHeader || !authHeader.startsWith("Bearer ")) {
        throw new UnAuthenticatedError("Unauthorize error!");
    }

    const token = authHeader.split(" ")[1];

    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET);

        //alternate code

        // const user = await User.findById(decoded.id).select("-password");
        // req.user = user;

        req.user = {
            id: decoded.id,
            email: decoded.email,
            name: decoded.name,
        };

        next();
    } catch (error) {
        throw new UnAuthenticatedError("Unauthorize error!");
    }
};
