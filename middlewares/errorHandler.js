const { StatusCodes } = require("http-status-codes");
const { CustomApiError } = require("../errors");

module.exports.errorHandler = (err, req, res, next) => {
    let customError = {
        statusCode: err.statusCode || StatusCodes.INTERNAL_SERVER_ERROR,
        status: err.status || false,
        message: err.message || "Something went wrong!",
    };

    // if (err instanceof CustomApiError) {
    //     return res.status(err.statusCode).json({
    //         status: err.status,
    //         message: err.message,
    //     });
    // }

    if (err.name && err.name === "ValidationError") {
        console.log(Object.values(err.errors));
        customError.message = Object.values(err.errors)
            .map((item) => item.message)
            .join(",");
        customError.statusCode = 400;
    }

    if (err.code && err.code === 11000) {
        (customError.message = `Duplicate value entered for ${Object.keys(err.keyValue)} field, please choose another value`), (customError.statusCode = StatusCodes.BAD_REQUEST);
    }

    if (err.name && err.name === "CastError") {
        customError.message = `No item found with id : ${err.value}`;
        customError.statusCode = StatusCodes.NOT_FOUND;
    }

    // return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
    //     status: false,
    //     message: "SOmething went wrong!",
    //     entity: err,
    // });

    return res.status(customError.statusCode).json({
        status: customError.status,
        message: customError.message,
    });
};
