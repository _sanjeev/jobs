const { BadRequestError } = require("./BadRequestError");
const { CustomApiError } = require("./CustomApiError");
const { NotFoundError } = require("./NotFoundError");
const { UnAuthenticatedError } = require("./UnauthenticatedError");

module.exports = {
    BadRequestError,
    CustomApiError,
    UnAuthenticatedError,
    NotFoundError,
};
